# -*- coding: utf-8 -*-

"""
    hoge_tools.py

    プログラム全体の説明を書きます。
    実行方法とか必要なライブラリとか、
    動作確認しているPythonのバージョンとか書くとよいと思いました
"""


from logging import getLogger, DEBUG

# ログの設定
logger = getLogger(__name__)
logger.setLevel(DEBUG)

# 定数です
NUM_SPHERE = 4
NUM_TRYSAIL = 3


def main():
    """ エントリポイントだよ。

    """
    logger.debug("mainの開始")
    func_fugafuga(1)

def func_fugafuga(parm1):
    """ func_fugafuga

            @param parm1: この引数はとくにつかいません
    """
    logger.error("エラーです！！")

"""
========================================
MyClass
========================================
"""
class MyClass:

    def __init__(self):
        """ コンストラクタの説明
        """
        self._pv_v = "インスタンス変数"

    def process(self, parm1):
        """ メソッドの説明
        """
        logger.debug("process")

    def _pv_process(self):
        """ メソッドの説明
        """
        logger.debug("_pv_process")


if __name__ == "__main__":
    main()