# -*- coding: utf-8 -*-
"""
    drow_image1.py

    ゲームを作りながら楽しく学べるPythonプログラミング
"""
import sys
import pygame
from pygame.locals import QUIT

pygame.init()
SURFACE = pygame.display.set_mode((400, 300))
FPSCLOCK = pygame.time.Clock()

def main():
    """ main routine """

    logo = pygame.image.load("jack01.jpg")

    while True:
        for event in pygame.event.get(QUIT):
            if event .type == QUIT:
                pygame.quit()
                sys.exit()

        SURFACE.fill((0, 0, 0))

        # blit(Surface, dest, area=None, spcial_flags=0) -> Rect
        #  Surface : コピー元となるSarface
        #  dest    : コピーする座標(左上)
        #  area    : コピーする領域(一部の未描画する時)
        #  special_flags : コピー時の演算方法
        # biltはコピー先のオブジェクトのメソッド

        # 左上が(20,50)の位置にロゴを描画
        SURFACE.blit(logo, (20, 20))


        pygame.display.update()
        FPSCLOCK.tick(3)

if __name__ == '__main__':
    main()
