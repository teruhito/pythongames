# -*- coding: utf-8 -*-
"""
    drow_rect1.py

    ゲームを作りながら楽しく学べるPythonプログラミング
"""
import sys
import pygame
from pygame.locals import QUIT, Rect

pygame.init()
SURFACE = pygame.display.set_mode((400, 300))
FPSCLOCK = pygame.time.Clock()

def main():
    """ main routine """

    while True:
        for event in pygame.event.get(QUIT):
            if event .type == QUIT:
                pygame.quit()
                sys.exit()

        SURFACE.fill((255, 255, 255))

        # rect(Surface, Color, Rect, Width=0) -> Rect
        #  Surface : 秒が対象となる画面(Surfaceオブジェクト)
        #  Color   : 色
        #  Rect    : 矩形の位置とサイズ
        #  Width   : 線の幅(省略時は塗りつぶし)

        # 赤：矩形（塗りつぶし）
        pygame.draw.rect(SURFACE, (255, 0, 0), (10, 20, 100, 50))

        # 赤：矩形（太さ３）
        pygame.draw.rect(SURFACE, (255, 0, 0), (150, 10, 100, 30),3 )

        # 青：矩形、Rectオブジェクト
        rect0 = Rect(200, 60, 140, 80)
        pygame.draw.rect(SURFACE, (0, 0, 255), rect0)

        # 黄：矩形、Rectオブジェクト
        rect1 = Rect((30, 160), (100, 50))
        pygame.draw.rect(SURFACE, (255, 255, 0), rect1)

        pygame.display.update()
        FPSCLOCK.tick(3)

if __name__ == '__main__':
    main()
