# -*- coding: utf-8 -*-
"""
    draw_ellipse.py

    ゲームを作りながら楽しく学べるPythonプログラミング
"""
import sys
import pygame
from pygame.locals import QUIT, Rect

pygame.init()
SURFACE = pygame.display.set_mode((400, 300))
FPSCLOCK = pygame.time.Clock()

def main():
    """ main routine """

    while True:
        for event in pygame.event.get(QUIT):
            if event .type == QUIT:
                pygame.quit()
                sys.exit()

        SURFACE.fill((255, 255, 255))

        # ellipse(Surface, Color, Rect, Width=0) -> Rect
        #  Surface : 秒が対象となる画面(Surfaceオブジェクト)
        #  Color   : 色
        #  Rect    : 楕円に外接する矩形の位置とサイズ
        #  Width   : 線の幅(省略時は塗りつぶし)

        # 赤
        pygame.draw.ellipse(SURFACE, (255, 0, 0), (50, 50, 140, 60))
        pygame.draw.ellipse(SURFACE, (255, 0, 0), (250, 30, 90, 90))

        # 緑
        pygame.draw.ellipse(SURFACE, (0, 255, 0), (50, 150, 110, 60), 5)
        pygame.draw.ellipse(SURFACE, (0, 255, 0), ((250, 130), (90, 90)), 20)

        pygame.display.update()
        FPSCLOCK.tick(3)

if __name__ == '__main__':
    main()
