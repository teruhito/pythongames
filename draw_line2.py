# -*- coding: utf-8 -*-
"""
    drow_line1.py

    ゲームを作りながら楽しく学べるPythonプログラミング
"""
import sys
import pygame
from pygame.locals import QUIT

pygame.init()
SURFACE = pygame.display.set_mode((400, 300))
FPSCLOCK = pygame.time.Clock()

def main():
    """ main routine """

    while True:
        for event in pygame.event.get(QUIT):
            if event .type == QUIT:
                pygame.quit()
                sys.exit()

        SURFACE.fill((0, 0, 0))

        # line(Surface, Color, start_pos, end_pos, Width=1) -> Rect
        #  Surface   : 秒が対象となる画面(Surfaceオブジェクト)
        #  Color     : 色
        #  start_pos : 始点
        #  end_pos   : 終点
        #  Width     : 線の幅(

        # 白：縦線
        for xpos in range(0, 400, 25):
            pygame.draw.line(SURFACE, 0xFFFFFF, (xpos, 0), (xpos, 300))

        # 白：横線
        for ypos in range(0, 300, 25):
            pygame.draw.line(SURFACE, 0xFFFFFF, (0, ypos), (400, ypos))

        pygame.display.update()
        FPSCLOCK.tick(3)

if __name__ == '__main__':
    main()
